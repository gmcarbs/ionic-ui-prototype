import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DropboxPageRoutingModule } from './dropbox-routing.module';

import { DropboxPage } from './dropbox.page';
import { MainHeaderModule } from 'src/app/components/main-header/main-header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DropboxPageRoutingModule,
    MainHeaderModule
  ],
  declarations: [DropboxPage]
})
export class DropboxPageModule {}

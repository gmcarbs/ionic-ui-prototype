import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-dropbox',
  templateUrl: './dropbox.page.html',
  styleUrls: ['./dropbox.page.scss'],
})
export class DropboxPage implements OnInit {
  constructor(private menu: MenuController) { }

  ngOnInit() {
  }

}

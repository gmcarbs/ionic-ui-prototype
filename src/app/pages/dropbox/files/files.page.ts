import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { FileItem } from './file-item';

@Component({
  selector: 'app-files',
  templateUrl: './files.page.html',
  styleUrls: ['./files.page.scss'],
})
export class FilesPage implements OnInit {
  items: FileItem[] = 
  [
    {
      thumbnail: "",
      type: 0,
      name: "folder 1",
      size: 10,
      dateModified: new Date("5/1/2021"),
      details: null
    },
    {
      thumbnail: "",
      type: 0,
      name: "folder 2",
      size: 10,
      dateModified: new Date("5/1/2021"),
      details: null
    },
    {
      thumbnail: "",
      type: 1,
      name: "Getting Started.pdf",
      size: 10,
      dateModified: new Date("5/1/2021"),
      details: "240 KB, modified 10 days ago"
    },
    {
      thumbnail: "",
      type: 1,
      name: "How to android.pdf",
      size: 10,
      dateModified: new Date("5/1/2021"),
      details: "10 MB, modified 5 days ago"
    },
    {
      thumbnail: "",
      type: 1,
      name: "coffee-and-code.txt",
      size: 10,
      dateModified: new Date("5/1/2021"),
      details: "760 KB, modified 3 days ago"
    }
  ];

  constructor(private menu: MenuController) { }

  ngOnInit() {
  }

  openMainMenu() {
    this.menu.enable(true, 'side');
    this.menu.open('side');
  }
}

export interface FileItem {
    thumbnail: string;
    name: string;
    type: number;
    size: number;
    dateModified: Date;
    details: string;
}
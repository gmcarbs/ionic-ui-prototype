import { Component, Input, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
})
export class MainHeaderComponent implements OnInit {

  @Input()
  public title: string;

  constructor(private menu: MenuController) { }

  ngOnInit() {}

  openMainMenu() {
    this.menu.enable(true, 'side');
    this.menu.open('side');
  }

}
